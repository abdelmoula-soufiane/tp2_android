package com.example.tp2;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {
    Cursor cursor;
    Wine wine;
    EditText name;
    EditText region;
    EditText localization;
    EditText climate;
    EditText publisher;
    Button save;
    WineDbHelper WineDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        WineDbHelper = new WineDbHelper(getApplicationContext());
        //recuperer le  Wine selectioner
        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();
//hbach recup donnée env
        wine = (Wine) extras.get("wineSelected");

//bach nrbt
        name = (EditText) findViewById(R.id.wineName) ;
        region = (EditText) findViewById(R.id.editWineRegion) ;
        localization = (EditText) findViewById(R.id.editLoc) ;
        climate = (EditText) findViewById(R.id.editClimate) ;
        publisher = (EditText) findViewById(R.id.editPlantedArea) ;


        if(wine!=null) {
            name.setText(wine.getTitle());
            region.setText(wine.getRegion());
            localization.setText(wine.getLocalization());
            climate.setText(wine.getClimate());
            publisher.setText(wine.getPlantedArea());
        }

        save = (Button) findViewById(R.id.button);

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//knverfyi mnin kiji win
                if(wine!=null) {
                    if(!name.getText().toString().isEmpty()) {
                        wine.setTitle(name.getText().toString());
                        wine.setRegion(region.getText().toString());
                        wine.setLocalization(localization.getText().toString());
                        wine.setClimate(climate.getText().toString());
                        wine.setPlantedArea(publisher.getText().toString());
                        WineDbHelper.updateWine(wine);

                        Toast.makeText(getApplicationContext(),"Edit Succefful",Toast.LENGTH_LONG).show();
                    }
                  else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("Le nom du vin doit etre non vide.\n\n\n\n");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                }
                else {
                    if(name.getText().toString().isEmpty()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("entrer le nom d'appelation");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                    else {
                        wine = new Wine(name.getText().toString(), region.getText().toString(),localization.getText().toString(), climate.getText().toString(), publisher.getText().toString());
                        if(!WineDbHelper.addWine(wine)){

                            AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                            builder.setMessage(" Ajout impossible \n Un vin portant le meme nom existe déjà dans la base de données");
                            builder.setCancelable(true);
                            builder.show();
                            return;

                        }
                        else
                            Toast.makeText(getApplicationContext(), "add succefull ", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
