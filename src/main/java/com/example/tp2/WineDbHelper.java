package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static java.sql.Types.ROWID;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "wine.db";
    public static final String TABLE_NAME = "cellar";
    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT NOT NULL, "+
                COLUMN_WINE_REGION + " TEXT, " +
                COLUMN_LOC + " TEXT, " +
                COLUMN_CLIMATE + " TEXT, " +
                COLUMN_PLANTED_AREA + " TEXT, " +
                //unique vars
                "UNIQUE (" + COLUMN_NAME + ", " + COLUMN_WINE_REGION+ ") ON CONFLICT ROLLBACK);");
    }

    public void clear(){

        this.getWritableDatabase().delete(TABLE_NAME, null, null);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME + ";"); // drops the old database
        db.delete(TABLE_NAME, null, null);
        onCreate(db);

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();


        // Inserting Row
        long rowID = 0;
        ContentValues bd=new ContentValues();
        bd.put(COLUMN_NAME, wine.getTitle());
        bd.put(COLUMN_WINE_REGION, wine.getRegion());
        bd.put(COLUMN_LOC, wine.getLocalization());
        bd.put(COLUMN_CLIMATE, wine.getClimate());
        bd.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        // call db.insert()

        rowID = db.insert(TABLE_NAME,null,bd);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;

        // updating row
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME,wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION,wine.getRegion());
        contentValues.put(COLUMN_LOC,wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE,wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        res = db.update(TABLE_NAME, contentValues, _ID + " = ? ", new String[] {Long.toString(wine.getId())});
        // call db.update()
        System.out.println("hello " + wine.getId());

        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {
                _ID,
                COLUMN_NAME,
                COLUMN_WINE_REGION,
                COLUMN_LOC,
                COLUMN_CLIMATE,
                COLUMN_PLANTED_AREA
        };
        Cursor cursor = db.query(TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null);

        // call db.query()
        //Cursor cursor = db.rawQuery("select * from " + TABLE_NAME,null );

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.delete(TABLE_NAME, _ID + " = ?", new String[] {String.valueOf(cursor.getLong(0))});
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }

    public static Wine cursorToWine(Cursor cursor) {

        // tsne3 wine min cursor
        int id_index = cursor.getColumnIndexOrThrow(_ID);
        int title_index = cursor.getColumnIndexOrThrow(COLUMN_NAME);
        int region_index = cursor.getColumnIndexOrThrow(COLUMN_WINE_REGION);
        int localization_index = cursor.getColumnIndexOrThrow(COLUMN_LOC);
        int climate_index = cursor.getColumnIndexOrThrow(COLUMN_CLIMATE);
        int plantedarea_index = cursor.getColumnIndexOrThrow(COLUMN_PLANTED_AREA);

        long id = (Long) cursor.getLong(id_index);
        System.out.print("cursoor : " + id +" --");
        String title = (String) cursor.getString(title_index);
        String region= (String) cursor.getString(region_index);
        String localization = (String) cursor.getString(localization_index);
        String climate = (String) cursor.getString(climate_index);
        String plantedarea = (String) cursor.getString(plantedarea_index);

        return new Wine (id, title, region, localization, climate, plantedarea);

    }
}

